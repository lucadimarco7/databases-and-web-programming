@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><img class="logo3" src="upload_media\photos\Collections.png"></div>

                <div class="card-body">
                    <h1>Collections of {{ Auth::user()->username }}</h1>
                        @foreach ($collections as $collection)

                        
                        <li class="collections"> 
                        <img src="{{asset($collection['image'])}}" width="200px"></a>
                        <div>{{ $collection['title']}}</div>
                        <a id="btn-detail" href="{{url('collections/'.$collection['id'])}}">
                            View details &raquo;
                        </a>
                        <a id="btn-delete" href="{{url('collections/delete/'.$collection['id'])}}">
                            Delete &CircleTimes;
                        </a>
                        </li>

                        @endforeach
                </div>

                <div class="card-body">
                    <a href="{{ route('home') }}">Return to home</a>
                </div>

            </div>

        </div>

    </div>

    
</div>
@endsection
