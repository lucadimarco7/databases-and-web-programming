@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><img class="logo4" src="{{ asset('collections\upload_media\photos\Contents.png')}}"></div>

                <div class="card-body">
                    <h1>Contents of {{$title}}</h1>
                    @foreach ($contents as $content)

                        <li class="contents">  
                        <img src="{{ $content->image}}" width="200px"></a>
                        <div>{{ $content->title}}</div>
                        
                        <p>
                        <a id="btn-detail" href="{{url('comics/'.$content['comicid'])}}">
                            View details &raquo;
                        </a>
                        <a id="btn-delete" href="{{url('comics/delete/'.$content['comicid'].'/'.$content['id'])}}">
                            Delete &CircleTimes;
                        </a>
                    </p>
                    </li>
                        @endforeach
                        
                </div>

                <div class="card-body">
                    <a href="{{ route('collections') }}">Return to collections</a>
                </div>

            </div>

        </div>

    </div>

    
</div>
@endsection
