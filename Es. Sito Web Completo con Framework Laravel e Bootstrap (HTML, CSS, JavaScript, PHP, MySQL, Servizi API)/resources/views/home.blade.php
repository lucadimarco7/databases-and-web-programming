@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><img class="logo" src="upload_media\photos\Home.png"></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Welcome {{ Auth::user()->name }}! 
                </div>

                <div class="card-body">
                  <li class="collection">  <a href="{{ route('collection') }}"><img src="collection\upload_media\photos\newcollection.png"><br>
                    Create new collection</a>
                </li>

                <li class="collection">
                    <a href="{{ route('collections') }}"><img src="collection\upload_media\photos\listcollection.png"><br>
                    Collections list</a>
                    </li>
                </div>
            </div>

        </div>
    </div>

    
</div>
@endsection
