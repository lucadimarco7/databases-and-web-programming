@extends('layouts.app')

@section('content')

    <div class="container text-center">
        @if(!empty($comics))
            <h1> Title: {{$comics[0]['title']}}</h1> <br/>
            <h3> ID: {{$comics[0]['id']}}</h3> <br/>

            @if(!empty($comics[0]['thumbnail']))
            @isset($comics[0]['images'][0]['path'])
            <img src="{{$comics[0]['images'][0]['path'].'.'.$comics[0]['images'][0]['extension']}}" height="350px"><br/> <br/>
            @else
            <img class="image" src="/upload_media/photos/noimage.png" height="350px">
            @endisset
            @endif

            <h2> Description: </h2> <br/>
            <p> 

                {{$comics[0]['description']}}

            </p>

            <br/>

            <h2> Characters: </h2> <br/>
            @if(!empty($characters))
                <ul>
            @endif
            @forelse ($characters as $character)
                <li>{{$character['name']}}</li>
            @empty
                No Characters!
            @endforelse
            @if(!empty($characters))
                </ul>
            @endif
        @endif

        <div>
            <a href="javascript:history.back()">Return to contents list</a>
        </div>
    </div>
@endsection