@extends('layouts.app')

@section('content')

    <div id="box-search-bar">
      <div class="container">
        @include('includes.search', ['searchValues' => $searchValues])
      </div>
    </div>    

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <b>Total Results:</b> {{$pagination['totalResults']}} <li>Click a comic to add to a collection</li><br/>
            </div>
        </div>
        @forelse ($comics as $comic)
            <div class="comics">
            @isset($comic['images'][0]['path'])
            <img class="image" src="{{$comic['images'][0]['path'].'.'.$comic['images'][0]['extension']}}">
            @else
            <img class="image" src="/upload_media/photos/noimage.png">
            @endisset
                <div class="title">
                     {{$comic['title']}}
                </div>
                <h2 class="comicid">
                     {{$comic['id']}}
                </h2>
            </div>
        @empty
            <p>No Comics</p>
        @endforelse
        
    </div>

    <div class="container">
        @include('includes.pagination', ['urlPagination' => $urlPagination, 'urlPaginationQuery' => $urlPaginationQuery])
    </div>
@endsection

