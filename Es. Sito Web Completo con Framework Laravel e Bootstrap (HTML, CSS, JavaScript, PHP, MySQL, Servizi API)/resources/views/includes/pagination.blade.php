@if(!empty($pagination['amountPages']))
<nav aria-label="Page navigation" class="text-center">
  <ul class="pagination">
    @if(!empty($pagination['prev']))
      <li>      
        <a href="{{url($urlPagination.'?page='.$pagination['prev'].$urlPaginationQuery)}}" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
        </a>      
      </li>
    @endif
    @if(($pagination['actual']+$pagination['pagesRange'])>$pagination['amountPages'])
      @for ($i = $pagination['amountPages']; $i >= ($pagination['amountPages'] - $pagination['pagesRange']); $i--)
        @if(intval(($pagination['amountPages'] - $pagination['pagesRange'])+($pagination['amountPages'] - $i)) > 0)                  
          <li>
            <a href="{{url($urlPagination.'?page='.intval(($pagination['amountPages'] - $pagination['pagesRange'])+($pagination['amountPages'] - $i)).$urlPaginationQuery)}}">
            {{intval(($pagination['amountPages'] - $pagination['pagesRange'])+($pagination['amountPages'] - $i))}}
            </a>
          </li>
        @endif
      @endfor
    @else
      @if(!empty($pagination['prev']))
        <li>
          <a href="{{url($urlPagination.'?page='.intval($pagination['actual'] - 1).$urlPaginationQuery)}}">
          {{intval($pagination['actual'] - 1)}}
          </a>
        </li>
      @endif
      @for ($i = $pagination['actual']; $i < ($pagination['actual']+$pagination['pagesRange']); $i++)
        <li><a href="{{url($urlPagination.'?page='.$i.$urlPaginationQuery)}}">{{$i}}</a></li>
      @endfor
    @endif
    @if(!empty($pagination['next']))
      <li>
        <a href="{{url($urlPagination.'?page='.$pagination['next'].$urlPaginationQuery)}}" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
        </a>
      </li>
    @endif
  </ul>
</nav>
@endif