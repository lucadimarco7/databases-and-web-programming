<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                <form id="center" method="POST" action="/comics">
                {{ csrf_field() }}
                    <input id="titleStartsWith" type="text" name="titleStartsWith" value="Title starts with..." autocomplete="titleStartsWith" onfocus=this.value="">
                    @error('titleStartsWith')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                    <button id="btn-search" type="submit">Send</button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
