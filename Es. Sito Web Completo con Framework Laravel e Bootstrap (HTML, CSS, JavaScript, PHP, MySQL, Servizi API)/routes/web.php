<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@home')->name('home');

Route::get('/collections/index','CollectionsController@index')->name('collections');
Route::get('/collections/fetch','CollectionsController@fetch')->name('fetch'); 
Route::get('/collections/create','CollectionsController@create')->name('collection');;
Route::post('/collectionsaction','CollectionsController@storeCollection');
Route::get('/collections/{id}', 'CollectionsController@show');
Route::get('/collections/delete/{id}', 'CollectionsController@delete');
Route::get('/contents/image','ContentsController@image')->name('image');

Route::get('/comic', 'ComicsController@index')->name('comic');
Route::get('/comics/{id}', 'ComicsController@show');
Route::get('/comics/delete/{comicid}/{id}', 'ComicsController@delete');
Route::any('/comics', 'ComicsController@comicsByName')->name('comics');

Route::get('/contents/store','ContentsController@store')->name('store');
