<?php
namespace Marvel\Exceptions;

use Exception;

class MarvelErrorException extends Exception
{


    public function __construct($message)
    {
        parent::__construct($message);
    }


}