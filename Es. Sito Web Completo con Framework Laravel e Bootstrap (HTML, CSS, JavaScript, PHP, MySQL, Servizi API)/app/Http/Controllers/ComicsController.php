<?php

namespace Marvel\Http\Controllers;

use Illuminate\Http\Request;
use Marvel\Http\Controllers\MarvelClient;
use Marvel\Collection;
use Marvel\Content;
use DB;

class ComicsController extends Controller
{
 /**
      
    */
    protected $marvelClient;

    /**
     * 
     *
     * @return void
     */
    public function __construct(){
        $this -> marvelClient = new MarvelClient();
    }

    /**
     * 
     *
     * @return \Illuminate\View\View
     */
    public function index(){
        
        $results = $this->marvelClient->doRequest('comics');
        
        $pagination = $this->marvelClient->pagination(1, $results['data']['total'], 20);

        $searchValues = ['', 'titleStartsWith'];

        return view('pages.comics', [
            'comics' => $results['data']['results'], 
            'pagination' => $pagination,
            'urlPagination' => 'comics',
            'urlPaginationQuery' => "",
            'searchValues' => $searchValues,
            "orderBy" => 'title'
        ]);
    }


    /**
     * 
     *
     * @param Illuminate\Http\Request $request
     * 
     * @return \Illuminate\View\View
     */
    public function comicsByName(Request $request)
    {
        $titleStartsWith = $request->input('titleStartsWith');
        
        $orderBy = $request->input('orderBy', 'title');

        $limit = $request->input('limit', 20);

        $page = $request->input('page', 1);

        if($page==1){
            $offset = 0;
        } else{
            $offset = ($page-1) * $limit;
        }

        $queryArray = [
            "titleStartsWith" => urlencode($titleStartsWith),
            "orderBy" => $orderBy,
            "limit" => $limit,
            "offset" => $offset,
        ];

        $query = $this->marvelClient->generateQuery($queryArray);

        $result = $this->marvelClient->doRequest('comics', $query);

        $searchValues = [$titleStartsWith, 'titleStartsWith'];
        

        if (!empty($result)){

            $pagination = $this->marvelClient->pagination($page, $result['data']['total'], 20);

            $urlPaginationQuery = $this->urlPaginationSearch($queryArray);

            return view('pages.comics', [
                'comics' => $result['data']['results'],
                'pagination' => $pagination,
                'urlPagination' => 'comics',
                'urlPaginationQuery' => $urlPaginationQuery,
                'searchValues' => $searchValues
            ]);

        } else{

            $pagination = $this->marvelClient->pagination($page, 0, 20);

            return view('pages.comics', [
                'comics' => ['data']['results'],
                'pagination' => $pagination,
                'urlPagination' => 'comics',
                'urlPaginationQuery' => "",
                'searchValues' => $searchValues
            ]);
        }
    }

    /**
     * 
     *
     * @param array $queryArray
     * 
     * @return string
     */
    public function urlPaginationSearch($queryArray)
    {
        
        return "&titleStartsWith=".$queryArray['titleStartsWith'];
            
        
    }

    /**
     * 
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
       
        $comics = $this->marvelClient->doRequest('comics/'.$id, '');

        $characters = $this->marvelClient->doRequest('comics/'.$id.'/characters', '');

        $searchValues = ['', 'titleStartsWith'];
        
        return view('pages.comic', [
            'comics' => $comics['data']['results'], 
            'characters' => $characters['data']['results'],
            'searchValues' => $searchValues
        ]);
    }

    public function delete($comicid,$id)
    {
        DB::table('contents')->where('comicid', $comicid)->where('id',$id)->delete();
        $result = DB::table('collections')->select('title')->where('id', $id)->first();
        $title=$result->title;
        $contents = Content::all()->where('id', $id);
        return view('collections.contents',compact('title'),compact('contents'));
    }

}
