<?php

namespace Marvel\Http\Controllers;

use Illuminate\Http\Request;
use Marvel\Exceptions\MarvelErrorException;


class MarvelClient extends Controller
{
   /**
     * 
     *
     * @var string
     */
    private $keyPublic;

    /**
     * 
     *
     * @var string
     */
    private $keyPrivate;

    /**
     * 
     *
     * @const string
     */
    const API_URL = 'http://gateway.marvel.com/v1/public/';


    /**
     * 
     *     
     */
    public function __construct() {        
        $this->keyPublic = env('MARVEL_PUBLIC_KEY', '2dca6f640a1d544b045f6572ceb12c42');
        $this->keyPrivate = env('MARVEL_PRIVATE_KEY', '61f814b7552051c2006e07d32b7532f7cecf43e2');
    }


    /**
     * 
     *
     * @param string 
     * @param string 
     *
     * @return array
     */
    public function doRequest($path, $query="")
    {
        $ts = time();

        $hash = md5($ts . $this->keyPrivate . $this->keyPublic);
 
        if(!empty($query)){
            $query = "?".$query."&";
        } else{
            $query = "?";
        }

        $urlFinal = self::API_URL.$path.$query."apikey=".$this->keyPublic."&ts=".$ts."&hash=".$hash;

        $curlOptions = [
            CURLOPT_URL => $urlFinal,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_VERBOSE => false,
            CURLOPT_HEADER => false,
            CURLOPT_FORBID_REUSE => true,
            CURLOPT_TIMEOUT => 0,
        ];

        $curl = curl_init();

        curl_setopt_array($curl, $curlOptions);

        $result = curl_exec($curl);

        $this -> checkCodeHTTTP(curl_getinfo($curl, CURLINFO_HTTP_CODE), $result);

        return json_decode($result, true);
    }

    /**
     * 
     * 
     * 
     *
     * @param int 
     * @param array 
     *
     * @return void 
     * @return App\Exceptions\MarvelErrorException
     */
    public function checkCodeHTTTP($HTTPCode, $result=[]){
        $result = json_decode($result, true);
        switch ($HTTPCode) {
            case 200:                
                break;

            case 404:
                throw new MarvelErrorException($result['status']);
                break;

            case 409:                    
                throw new MarvelErrorException($result['status']);
                break;
                    
            default:
                throw new MarvelErrorException("Unexpected Error");
                break;
        }
    }

    /**
     * 
     * 
     * @param array $queryArray 
     *
     * @return string $query
    */
    public function generateQuery($queryArray)
    {
        $i = 0;
        $query ="";

        foreach ($queryArray as $key => $value) {
            if ($i==0){
                if ($value!=""){
                    $query = $query.$key."=".$value;
                    $i++;
                }
            }
            else{
                if ($value!=""){
                    $query = $query."&".$key."=".$value;
                }
                
            }            
        }

        return $query;
    }

    /**
     * 
     * 
     * @param int $page 
     * @param int $total 
     * @param int $itemsPage 
     * @param int $pagesRange 
     *
     * @return array
    */
    public function pagination($page=1, $total=0, $itemsPage=20, $pagesRange=10){
        if($total > 0){
            $amountPages = intval(ceil($total/$itemsPage));
            if ($page==0){
                $page = 1;
            }
            $offset = $page * $itemsPage;

            $prevPage = $page - 1;
            if ($amountPages==$page){
                $nextPage = 0;    
            } else{
                $nextPage = $page + 1;
            }
        } else {
            $prevPage = 0;
            $page = 0;
            $nextPage = 0;
            $amountPages = 0;
            $offset = 0;
            $itemsPage = 0;
        }


        return [
            "prev" => $prevPage,
            "actual" => $page,
            "next" => $nextPage,
            "amountPages" => $amountPages,
            "offset" => $offset,
            "itemsPage" => $itemsPage,
            "pagesRange" => $pagesRange,
            "totalResults" => $total
        ];
    }


}
