<?php

namespace Marvel\Http\Controllers;

use Illuminate\Http\Request;
use Marvel\Content;
use DB;

class ContentsController extends Controller
{
    //
    public function store(Request $request){
        $comic = DB::table('contents')->where('id',$request->id)->where('comicid',$request->comicid)->select('comicid')->get();
        if($comic!="[]"){
            return json_encode("Already exist in this collection");
        }else{
            $contents = new Content();
 
            $contents->id = $request->id;
            $contents->title = $request->title;
            $contents->image = $request->image;
            $contents->comicid = $request->comicid;

            $contents->save();

            // Return as json
            return json_encode("success");
        }
        }

    public function image(Request $request){
        $image = DB::table('collections')->select('image')->where('id',$request->id)->first();
        if($image->image=="upload_media\\photos\\image.png"){
            $collection = DB::table('collections')->where('id',$request->id)->update(['image'=>$request->image]);}
      
          // Return as json
          return json_encode("success");
          }

}
