<?php

namespace Marvel\Http\Controllers;

use Illuminate\Http\Request;
use Marvel\Collection;
use Marvel\Content;
use Auth;
use DB;

class CollectionsController extends Controller
{
    public function index(){
        $username=Auth::user()->username;
        $collections = DB::table('collections')->where('username',$username)->get();
        $collections=json_decode($collections,true);
        return view('collections.index',compact('collections'));
    }
 
    public function select(){
 
        $collections = Collection::all();
 
        return view('collections.select',compact('collections'));
    }

    public function create(){
        return view('collections.create');
    }
 
    public function storeCollection(){
 
        $collections = new Collection();
 
        $collections->username = Auth::user()->username;
        $collections->title = request('title');
        $collections->image = 'upload_media\photos\image.png';

        $collections->save();
 
        return redirect('/home');
 
    }

    public function fetch(){
        $username=Auth::user()->username;
        $collections = DB::table('collections')->where('username', $username)->get();
        
        // Return as json
        return json_encode($collections);
        }

    public function show($id){
        $result = DB::table('collections')->select('title')->where('id', $id)->first();
        $title=$result->title;
        $contents = Content::all()->where('id', $id);
        return view('collections.contents',compact('title'),compact('contents'));
    }

    public function delete($id)
    {
        DB::table('collections')->where('id', $id)->delete();
        $username=Auth::user()->username;
        $collections = DB::table('collections')->where('username',$username)->get();
        $collections=json_decode($collections,true);
 
        return view('collections.index',compact('collections'));
    }

}
