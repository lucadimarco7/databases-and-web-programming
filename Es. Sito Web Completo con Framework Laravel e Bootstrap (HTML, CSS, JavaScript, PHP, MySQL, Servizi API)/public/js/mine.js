let arraycontent=[];
let element=document.querySelectorAll('.comics');
for(box of element){
    box.addEventListener('click',addtocollection);
}

function addtocollection(){
    let elem=this.getElementsByClassName("image");
    let src=$(elem).attr("src");
    let elem2=this.getElementsByClassName("title");
    let title=$(elem2).html();
    let elem3=this.getElementsByClassName("comicid");
    let comicid=$(elem3).html();

    let request = $.get('/collections/fetch');

    // When it's done
    request.done(function(response) {
        arraycontent=JSON.parse(response);
        if(arraycontent.length==0){
            alert("No collection created");
            window.location.href = "collections/create";}
        let container=document.querySelectorAll('.container');
        for(evento of container){
            evento.classList.add('none');
        }

        let body=document.querySelector('body');
        let text=document.createElement("li");
        $(text).css("color","lightgrey").css("margin-bottom","20px");
        text.textContent="Select the collection where to add the content";
        text.classList.add('toremove')
        body.appendChild(text);
        for(let i=0;i<arraycontent.length;i++){
            let box=document.createElement("li");
            let button=document.createElement("div");
            let id=document.createElement("div");
            id.textContent=arraycontent[i].id;
            id.classList.add('id');
            $(id).css("display","none");
            box.classList.add('button');
            box.classList.add('toremove');
            box.addEventListener('mouseover',function(){
                box.classList.remove('button');
                box.classList.add('mouseover');
            });
            box.addEventListener('mouseout',function(){
                box.classList.remove('mouseover');
                box.classList.add('button');
            });
            button.textContent=arraycontent[i].title;
            
            
            box.appendChild(button);
            box.appendChild(id);
            body.appendChild(box);
            box.addEventListener("click",selectcollection);
        }

        function selectcollection(){
            let elem4=this.getElementsByClassName("id");
            let id=$(elem4).html();
            
            $.ajax({
                url: '/contents/store', 
                type:'get',
                data: { "id":id,"image": src, "title": title, "comicid":comicid 
              },
                success: function(response){
                    alert(response);
                }
            });

            $.ajax({
                url: '/contents/image', 
                type:'get',
                data: { "id":id,"image": src
              },
                success: function(response){
                    console.log(response);
                }
            });

            $('.toremove').remove();
            for(evento of container){
                evento.classList.remove('none');
            }
        };
    });
    
}

let comics=document.querySelectorAll('.comics');
for(event of comics){
    event.addEventListener('mouseover',function(){
        this.classList.remove('comics');
        this.classList.add('mouseover');
    })
    event.addEventListener('mouseout',function(){
        this.classList.remove('mouseover');
        this.classList.add('comics');
    })
}

let collections=document.querySelectorAll('.collection');
for(event of collections){
    event.addEventListener('mouseover',function(){
        this.classList.remove('collection');
        this.classList.add('mouseover');
    })
    event.addEventListener('mouseout',function(){
        this.classList.remove('mouseover');
        this.classList.add('collection');
    })
}