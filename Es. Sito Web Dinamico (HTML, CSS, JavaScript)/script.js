/* TODO: inserite il codice JavaScript necessario a completare il MHW! */
const CHECKED = "images/checked.png";
const UNCHECKED= "images/unchecked.png";

var r1;
var r2;
var r3;

const elemento = document.querySelectorAll('.choice-grid div');
for(const box of elemento){
    box.addEventListener("click",funzione);
}

function reset(){
    const elemento = document.querySelectorAll('.choice-grid div');
for(const box of elemento){
    box.addEventListener("click",funzione);
}

}

const elemento1 = document.querySelectorAll('.button');
for(box1 of elemento1){
    box1.addEventListener("click",funzione1);
}

const elemento2 = document.querySelectorAll('.button');
const elemento3 = document.querySelectorAll('.button');
for(const box2 of elemento2){
    box2.addEventListener("mouseover",funzione2);
}
for(const box3 of elemento3){
    box3.addEventListener("mouseout",funzione3);
}


function funzione(event){
    const container=event.currentTarget;
    var a=container.dataset.choiceId;
    var b=container.dataset.questionId;
    var c;

    if(b==='one'){
        r1=a;
        
        $(".uno div").css("opacity","0.6");
        $(this).css("opacity","100");
        $(this).css("background-color","#cfe3ff");
        $(".uno img.checkbox").attr("src",UNCHECKED);
        const image=container.querySelector('.checkbox');
        image.src=CHECKED;
        
    }else if(b==='two'){
        r2=a;
        
        $(".due div").css("opacity","0.2");
        $(this).css("opacity","100");
        $(this).css("background-color","#cfe3ff");
        $(".due img.checkbox").attr("src",UNCHECKED);
        const image=container.querySelector('.checkbox');
        image.src=CHECKED;
    }else{
        r3=a;
        
        $(".tre div").css("opacity","0.2");
        $(this).css("opacity","100");
        $(this).css("background-color","#cfe3ff");
        $(".tre img.checkbox").attr("src",UNCHECKED);
        const image=container.querySelector('.checkbox');
        image.src=CHECKED;
    }

    if(r1!==undefined && r2!==undefined && r3!==undefined){
        calcolarisultato(r1,r2,r3);
        
    for(item of elemento){item.removeEventListener("click",funzione);}

    }

    console.log(r1);
    console.log(r2);
    console.log(r3);

}


function calcolarisultato(){
    if(arguments[1]===arguments[2]){
        if(arguments[1]==="blep"){
            $("h1#titolo").text(RESULTS_MAP.blep.title);
            $("div#testo").text(RESULTS_MAP.blep.contents);}
        else if(arguments[1]==="burger"){
            $("h1#titolo").text(RESULTS_MAP.burger.title);
            $("div#testo").text(RESULTS_MAP.burger.contents);}
        else if(arguments[1]==="cart"){
            $("h1#titolo").text(RESULTS_MAP.cart.title);
            $("div#testo").text(RESULTS_MAP.cart.contents);}
        else if(arguments[1]==="dopey"){
            $("h1#titolo").text(RESULTS_MAP.dopey.title);
            $("div#testo").text(RESULTS_MAP.dopey.contents);}
        else if(arguments[1]==="happy"){
            $("h1#titolo").text(RESULTS_MAP.happy.title);
            $("div#testo").text(RESULTS_MAP.happy.contents);}
        else if(arguments[1]==="nerd"){
            $("h1#titolo").text(RESULTS_MAP.nerd.title);
            $("div#testo").text(RESULTS_MAP.nerd.contents);}
        else if(arguments[1]==="shy"){
            $("h1#titolo").text(RESULTS_MAP.shy.title);
            $("div#testo").text(RESULTS_MAP.shy.contents);}
        else if(arguments[1]==="sleeping"){
            $("h1#titolo").text(RESULTS_MAP.sleeping.title);
            $("div#testo").text(RESULTS_MAP.sleeping.contents);}
        else if(arguments[1]==="sleepy"){
            $("h1#titolo").text(RESULTS_MAP.sleepy.title);
            $("div#testo").text(RESULTS_MAP.sleepy.contents);}
    }else{
            if(arguments[0]==="blep"){
                $("h1#titolo").text(RESULTS_MAP.blep.title);
                $("div#testo").text(RESULTS_MAP.blep.contents);}
            else if(arguments[0]==="burger"){
                $("h1#titolo").text(RESULTS_MAP.burger.title);
                $("div#testo").text(RESULTS_MAP.burger.contents);}
            else if(arguments[0]==="cart"){
                $("h1#titolo").text(RESULTS_MAP.cart.title);
                $("div#testo").text(RESULTS_MAP.cart.contents);}
            else if(arguments[0]==="dopey"){
                $("h1#titolo").text(RESULTS_MAP.dopey.title);
                $("div#testo").text(RESULTS_MAP.dopey.contents);}
            else if(arguments[0]==="happy"){
                $("h1#titolo").text(RESULTS_MAP.happy.title);
                $("div#testo").text(RESULTS_MAP.happy.contents);}
            else if(arguments[0]==="nerd"){
                $("h1#titolo").text(RESULTS_MAP.nerd.title);
                $("div#testo").text(RESULTS_MAP.nerd.contents);}
            else if(arguments[0]==="shy"){
                $("h1#titolo").text(RESULTS_MAP.shy.title);
                $("div#testo").text(RESULTS_MAP.shy.contents);}
            else if(arguments[0]==="sleeping"){
                $("h1#titolo").text(RESULTS_MAP.sleeping.title);
                $("div#testo").text(RESULTS_MAP.sleeping.contents);}
            else if(arguments[0]==="sleepy"){
                $("h1#titolo").text(RESULTS_MAP.sleepy.title);
                $("div#testo").text(RESULTS_MAP.sleepy.contents);}
            }
        
    const visibile = document.querySelector('#risultato');
    visibile.classList.remove('hidden');

}


  function funzione1() {
    $(".choice-grid div").css("opacity","100");
    $(".choice-grid div").css("background-color","#f4f4f4");
    $(".choice-grid img.checkbox").attr("src",UNCHECKED);
    r1=undefined;
    r2=undefined;
    r3=undefined;
    const visibile = document.querySelector('#risultato');
    visibile.classList.add('hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    reset();
}

function funzione2(){
    $(".button").css("background-color","#e0e0e0");
}

function funzione3(){
    $(".button").css("background-color","#cecece");
}
