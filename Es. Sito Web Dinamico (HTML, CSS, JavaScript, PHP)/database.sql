create database mini_homework3;
use mini_homework3;

create table argomento(
id varchar(20) primary key,
testo text
);

create table domanda(
id varchar(20) primary key,
testo text,
argomento_id varchar(20),
index newargomento_id(argomento_id),
foreign key(argomento_id) references argomento(id) on update cascade
);

create table contenuto(
id varchar(20),
img_url text,
domanda_id varchar(20),
index newdomanda_id(domanda_id),
foreign key(domanda_id) references domanda(id) on update cascade,
primary key(id,domanda_id)
);

create table risultato(
chiave varchar(20) primary key,
titolo text,
testo text
);




insert into argomento(id,testo) values("personaggi","Scegliete un personaggio e vi dirò chi siete");

insert into domanda(id,testo,argomento_id) values("one","Scegli un supereroe:","personaggi");
insert into domanda(id,testo,argomento_id) values("two","Scegli un personaggio neutrale:","personaggi");
insert into domanda(id,testo,argomento_id) values("three","Scegli un supercattivo:","personaggi");

insert into contenuto(id,img_url,domanda_id) values("blep","images/1_1.jpg","one");
insert into contenuto(id,img_url,domanda_id) values("happy","images/1_2.jpg","one");
insert into contenuto(id,img_url,domanda_id) values("sleeping","images/1_3.jpg","one");
insert into contenuto(id,img_url,domanda_id) values("dopey","images/1_4.jpg","one");
insert into contenuto(id,img_url,domanda_id) values("burger","images/1_5.jpg","one");
insert into contenuto(id,img_url,domanda_id) values("cart","images/1_6.jpg","one");
insert into contenuto(id,img_url,domanda_id) values("nerd","images/1_7.jpg","one");
insert into contenuto(id,img_url,domanda_id) values("shy","images/1_8.jpg","one");
insert into contenuto(id,img_url,domanda_id) values("sleepy","images/1_9.jpg","one");

insert into contenuto(id,img_url,domanda_id) values("blep","images/2_1.jpg","two");
insert into contenuto(id,img_url,domanda_id) values("happy","images/2_2.jpg","two");
insert into contenuto(id,img_url,domanda_id) values("sleeping","images/2_3.jpg","two");
insert into contenuto(id,img_url,domanda_id) values("dopey","images/2_4.jpg","two");
insert into contenuto(id,img_url,domanda_id) values("burger","images/2_5.jpg","two");
insert into contenuto(id,img_url,domanda_id) values("cart","images/2_6.jpg","two");
insert into contenuto(id,img_url,domanda_id) values("nerd","images/2_7.jpg","two");
insert into contenuto(id,img_url,domanda_id) values("shy","images/2_8.jpg","two");
insert into contenuto(id,img_url,domanda_id) values("sleepy","images/2_9.jpg","two");

insert into contenuto(id,img_url,domanda_id) values("blep","images/3_1.jpg","three");
insert into contenuto(id,img_url,domanda_id) values("happy","images/3_2.jpg","three");
insert into contenuto(id,img_url,domanda_id) values("sleeping","images/3_3.jpg","three");
insert into contenuto(id,img_url,domanda_id) values("dopey","images/3_4.jpg","three");
insert into contenuto(id,img_url,domanda_id) values("burger","images/3_5.jpg","three");
insert into contenuto(id,img_url,domanda_id) values("cart","images/3_6.jpg","three");
insert into contenuto(id,img_url,domanda_id) values("nerd","images/3_7.jpg","three");
insert into contenuto(id,img_url,domanda_id) values("shy","images/3_8.jpg","three");
insert into contenuto(id,img_url,domanda_id) values("sleepy","images/3_9.jpg","three");

insert into risultato(chiave,titolo,testo) values("blep","Ottimo valutatore di carattere","Il vostro sguardo nasconde lo spirito da segugio che è in voi, e sa leggere gli altri e i loro desideri. Ridete quando siete confusi e sembrate straniti quando le cose sono divertenti. Vi piacciono le frasi brevi. Avete sempre voluto avere un giardino; purtroppo, le piante non sono facili da capire come gli umani, e l'ultima volta che vi siete presi  cura di un fiore non è andata così bene. Ad ogni modo, vi prendete buona cura dei vostri amici, che sono fortunati ad avervi.");
insert into risultato(chiave,titolo,testo) values("happy","Una forza positiva nel mondo","Siete ottimisti e fiduciosi della vostra abilità di migliorare le cose, che siano esse un gruppo di persone, un processo lavorativo, o voi stessi. E' difficile decidere se siete introversi o estroversi. Avete tanti libri che intendete leggere, e la prossima settimana ne inizierete uno. Quando vi svegliate la mattina, avete difficoltà a ricordare cosa avete letto. Vi piace il suono della pioggia ma non starci in mezzo. Avete paura di non riuscire a fare abbastanza, mente tutti pensano che facciate più di loro.");
insert into risultato(chiave,titolo,testo) values("sleeping","Liberi e indipendenti","Trovate energia nel fare le cose che ritenete importanti, e diventate irrequieti se vi sembra di essere sempre la stessa persona. Siete bravi ad articolare le idee. Gli orologi sono una delle cose che vi piacciono di meno. Vi separate sempre dal vostro gruppo per esplorare un posto nuovo, e fortunatamente avete un ottimo senso dell'orientamento. Avete un talento nascosto che pochi conoscono, ma tranquilli, manterrò il segreto.");
insert into risultato(chiave,titolo,testo) values("dopey","Scherzosi ma con un lato riflessivo","Vi piace correre, saltare, scattare, camminare, fare jogging e andare sul monociclo. Il vostro nome utente online è \"L0LxXXxNawt_4_daag_96\". Occasionalmente aggiungete delle faccine vicino alla vostra firma. Indossate segretamente una catenina d'oro con un ciondolo a forma di guanti da boxe e un'etichetta con scritto \"Rocky\". Quando siete soli, però, riflettete sulla crescente commercializzazione della capacità creativa umana e vi piace ripetere Marx mentre camminate nei boschi.");
insert into risultato(chiave,titolo,testo) values("burger","Deciso e determinato","E' più probabile che siate voi a influenzare le persone intorno piuttosto che il contrario. La qualità del vostro lavoro conta molto per voi. Siete dei pensatori visuali, e sapete trovare analogie tra argomenti diversi. Vi piace condividere scherzi con gli amici. Siete puntigliosi sul cibo, ma vi piace provare un nuovo piatto almeno una volta. Credete che la contraddizione sia alla base della vostra anima. A volte non ve ne rendete conto, ma molte persone vi ammirano.");
insert into risultato(chiave,titolo,testo) values("cart","Perfezionista con un lato tenero","Siete calorosi a bravi a migliorare la giornata degli altri. Segretamente, vi piace l'idea di essere dei ribelli, ma la maggior parte di questa ribellione finisce nella vostra testa. Vi piace fare diagrammi di flusso. Volete sempre finire tutto quello che avete in programma per la giornata, e quando siete concentrati è difficile interrompervi. E' importante per voi aver un sfogo creativo e un'indipendenza personale. Siete fortemente motivati, il che è spesso fonte di ispirazione per gli altri.");
insert into risultato(chiave,titolo,testo) values("nerd","Mentalmente aperti e dal cuore grande","Siete fan di Harry Potter, e siete stati scelti per riparare ai torti del mondo e riportare l'armonia nella Terra di Mezzo. Vi piace leggere biografie storiche e fare test di personalità che confermino che la vostra personalità sia più rara di un fiocco di neve sul dorso di un giaguaro. Avete poteri magici, ma vi consiglio di non metterli sul curriculum, perchè la gente potrebbe non capirvi.");
insert into risultato(chiave,titolo,testo) values("shy","A volte timidi, ma segretamente avventurosi","Siete delle persone molto riflessive, sia quando pensate agli altri che quando pensate a delle idee. Vi piace la natura, in particolare il modo in cui la luce filtra tra le foglie. In un gruppo, non siete quasi mai la prima persona a parlare, ma quando lo fate gli altri vogliono che continuiate. Date più valore al futuro che al passato. Non vi piacciono i giochi di parole e vi piacciono le note scritte a mano; i giochi di parole scritti a mano vanno bene. Non avete paura di affrontare nuovo sfide, il che è molto bello.");
insert into risultato(chiave,titolo,testo) values("sleepy","Piu' forti di quanto pensiate","La chiamate \"meditazione\", ma è piu' \"chiudere gli occhi tra una sveglia e l'altra\". Lo chiamate \"sollevamento pesi\", ma è più \"una buona scusa per bere Gatorade\". Le vostre battute hanno spesso un buon tempismo e piacciono a tutti. Trascendete le difficoltà della vita, schivandole agilmente. Mangiate i biscotti della fortuna, ma non leggete mai i bigliettini perchè siete già sicuri di voi. Fare pisolini è uno dei vostri talenti, ma quando vi concentrate, vi dedicate al 100% a fare qualcosa.");
