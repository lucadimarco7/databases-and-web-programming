/* TODO: inserite il codice JavaScript necessario a completare il MHW! */
const CHECKED = "images/checked.png";
const UNCHECKED= "images/unchecked.png";

var r1;
var r2;
var r3;

const elemento = document.querySelectorAll('.choice-grid div');
for(const box of elemento){
    box.addEventListener("click",funzione);
}

function reset(){
for(const box of elemento){
    box.addEventListener("click",funzione);
}
}



function funzione(event){
    const container=event.currentTarget;
    var a=container.dataset.choiceId;
    var b=container.dataset.questionId;
    var c;

    if(b==='one'){
        r1=a;
        
        $(".uno div").css("opacity","0.6");
        $(this).css("opacity","100");
        $(this).css("background-color","#cfe3ff");
        $(".uno img.checkbox").attr("src",UNCHECKED);
        const image=container.querySelector('.checkbox');
        image.src=CHECKED;
        
    }else if(b==='two'){
        r2=a;
        
        $(".due div").css("opacity","0.2");
        $(this).css("opacity","100");
        $(this).css("background-color","#cfe3ff");
        $(".due img.checkbox").attr("src",UNCHECKED);
        const image=container.querySelector('.checkbox');
        image.src=CHECKED;
    }else{
        r3=a;
        
        $(".tre div").css("opacity","0.2");
        $(this).css("opacity","100");
        $(this).css("background-color","#cfe3ff");
        $(".tre img.checkbox").attr("src",UNCHECKED);
        const image=container.querySelector('.checkbox');
        image.src=CHECKED;
    }

    if(r1!==undefined && r2!==undefined && r3!==undefined){
        calcolarisultato(r1,r2,r3);
        
    for(item of elemento){item.removeEventListener("click",funzione);}

    }
}


function calcolarisultato(){
    var ris;
    if(arguments[1]===arguments[2]){
        ris=arguments[1];
    }else{
        ris=arguments[0];
    }

    $.ajax({
      url: 'php2.php', 
      type:'post',
      data: { aaa: ris },
     success: function(result) {
        var obj = JSON.parse(result);
        var  titolo=obj.titolo;
        var  testo=obj.testo;
        
              const risultato=document.querySelector("#risultato");
              const titolo1=document.createElement("h1");
              titolo1.textContent=titolo;
              titolo1.classList.add('toremove');
              const paragrafo=document.createElement("p");
              paragrafo.textContent=testo;
              paragrafo.classList.add('toremove');
              const button=document.createElement("button");
              button.textContent="Ricomincia il quiz";
              button.classList.add('toremove');
              button.classList.add('button');
              risultato.appendChild(titolo1);
              risultato.appendChild(paragrafo);
              risultato.appendChild(button);
              let bottone=document.querySelector('button');
              bottone.addEventListener("click",funzione1);
              bottone.addEventListener("mouseover",funzione2);
              bottone.addEventListener("mouseout",funzione3);
              risultato.classList.remove('hidden');
              
      }
    });
        
}


  function funzione1() {
    $(".choice-grid div").css("opacity","100");
    $(".choice-grid div").css("background-color","#f4f4f4");
    $(".choice-grid img.checkbox").attr("src",UNCHECKED);
    r1=undefined;
    r2=undefined;
    r3=undefined;
    const visibile = document.querySelector('#risultato');
    visibile.classList.add('hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $(".toremove").remove();
    reset();
}

function funzione2(){
    $(".button").css("background-color","#e0e0e0");
}

function funzione3(){
    $(".button").css("background-color","#cecece");
}

function onJSON(json){
    const intestazione=document.querySelector(".intestazione");
    const domanda1=document.querySelector("#uno");
    const domanda2=document.querySelector("#due");
    const domanda3=document.querySelector("#tre");
    const scelte=document.querySelectorAll(".scelte");

    for(evento of json){
        const h1=document.createElement("h1");
        const immagine=document.createElement("img");
        const checkbox=document.createElement("img");
        if(evento.id==="personaggi"){
            h1.textContent=evento.testo;
            intestazione.appendChild(h1);
        }

        if(evento.id==="one"){
            h1.textContent=evento.testo;
            domanda1.appendChild(h1);
        }

        if(evento.id==="two"){
            h1.textContent=evento.testo;
            domanda2.appendChild(h1);
        }

        if(evento.id==="three"){
            h1.textContent=evento.testo;
            domanda3.appendChild(h1);
        }

        for(let i=0;i<scelte.length;i++){
            let idchoice=scelte[i].dataset.choiceId;
            let idquest=scelte[i].dataset.questionId;
            
            if((idchoice===evento.id)&&(idquest===evento.domanda_id)){
                
                immagine.src=evento.img_url;
                checkbox.src=UNCHECKED;
                checkbox.classList.add("checkbox");
                scelte[i].appendChild(immagine);
                scelte[i].appendChild(checkbox);
            }
    }
}
}


function onResponseDomande(response){
    return response.json();
}

fetch("php.php").then(onResponseDomande).then(onJSON);



