//prelevo dati dal form
const div=document.querySelector(".reg_user");
const nome=document.querySelector(".nome");
const cognome=document.querySelector(".cognome");
const email=document.querySelector(".e-mail");
const nomeutente=document.querySelector(".nomeutente");
const password=document.querySelector(".password");
const password2=document.querySelector(".password2");

div.addEventListener("click",funzione2);
nomeutente.addEventListener("blur",funzione4);
function funzione2(event){
    event.preventDefault();
    let errors=[];
    //verifica se i campi sono vuoti
    if(nome.value===""||cognome.value===""||email.value===""||nomeutente.value===""||password.value===""||password2.value===""){
        alert("Riempire tutti i campi");
        errors.push('1');
    }

    //verifica se le due password coincidono
    if(password.value!==password2.value){
        alert("Password non coincidenti");
        errors.push('1');
    }

    //verifica se l'email contiene la chiocciola
    var a=email.value.indexOf("@");
    if(a===-1){
        alert("E-mail non valida");
        errors.push('1');
    }
    
    //invio tramite ajax il nome utente per vedere se esiste già
    $.ajax({
        url: 'server.php', 
        type:'post',
        data: {"username":nomeutente.value
      },
       success: function(result) {
        if(result==0){
            alert("Nome utente esistente");
        }else{
            funzione3();
        }
    }})

    //se non ci sono errori invio tutti i dati necessari alla registrazione
    function funzione3(){
    if(errors.length==0){
    $.ajax({
        url: 'server.php', 
        type:'post',
        data: {"nome":nome.value,"cognome":cognome.value,"email":email.value,"nomeutente":nomeutente.value,"password":password.value
      }
 })
    window.location.replace("http://localhost/homework/home/home.php");    
}
}
}


function funzione4(){
    $.ajax({
        url: 'server.php', 
        type:'post',
        data: {"username":nomeutente.value
      },
       success: function(result) {
        if(result==0){
            $(nomeutente).css("background-color","red");
        }else{
            $(nomeutente).css("background-color","darkblue");
        }
    }})
}