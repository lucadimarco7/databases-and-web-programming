create database homework;
use homework;

create table utenti(
nome varchar(20),
cognome varchar(20),
email varchar(50),
nomeutente varchar(20),
password  varchar(20),
primary key(nomeutente)
);

create table collezione(
id int auto_increment,
url varchar(200),
titolo varchar(100),
username varchar(20),
index a(username),
foreign key(username) references utenti(nomeutente) on update cascade,
primary key(id)
);

create table contenuto(
id int,
url varchar(200),
titolo varchar(100),
prezzo varchar(10),
pagine varchar(900),
descrizione text,
index a(id),
foreign key(id) references collezione(id) on update cascade,
primary key(id,titolo)
);

 DELIMITER $$
    CREATE TRIGGER beforedelete 
        BEFORE DELETE ON collezione
        FOR EACH ROW 
    BEGIN
        delete from contenuto where id=OLD.id;
    END$$
    DELIMITER ;