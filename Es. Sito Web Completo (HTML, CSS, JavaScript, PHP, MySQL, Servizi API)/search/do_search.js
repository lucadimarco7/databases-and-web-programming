//funzioni per il cambio colore dei vari menù
const elem1=document.querySelectorAll("#menu p");
for(box of elem1){
    box.addEventListener("mouseover",function(){
        $(this).css("color","black").css("background-color"," rgb(25, 98, 255)").css("border-left","8px solid black");})
    box.addEventListener("mouseout",function(){
        $(this).css("border-left","none").css("color","white").css("background-color","rgba(0, 0, 0, 0)");})
}

const input=document.querySelector(".barra");
input.addEventListener("focus",function(){
  input.value="";
})

//creo gli array contenenti i dati dei contenuti
let arraytitle=[];
let arrayid=[];

//funzione che richiede i contenuti API
$("form").on("submit",function funzioneapi(event){
  event.preventDefault();
  $("form").off("submit",funzioneapi);
  $(".results").remove();
  
  // ricerca di un contenuto tramite l'iniziale  
  let testo  = event.currentTarget[0].value; 

  $.ajax({
    url: 'http://localhost/homework/search/do_search.php', 
    type:'post',
    data: { "testo": testo,
  }
    ,success: function(result)  {
        //creazione della sezione con i contenuti ritornati dalla richiesta API       
        let response=JSON.parse(result);
        const body=document.querySelector("body");
        const div=document.createElement("div");
        div.classList.add('results');
        body.appendChild(div);
  
        let results = response.data.results;
        let resultsLen = results.length;
        let output = '<ul>';

        for(let i=0; i<resultsLen; i++){
          if(results[i].images.length > 0) {
            let imgPath = results[i].images[0].path + '/portrait_uncanny.' + results[i].images[0].extension;
            output += '<li><img class="immagine" src="' + imgPath + '"><br><div class="titolo">'+results[i].title+'</div><br><testo>clicca sul box per aggiungere ad una raccolta</testo>';
            output += '<div class="prezzo , hyde">'+results[i].prices[0].price+'</div>';
            output += '<div class="pagine , hyde">'+results[i].pageCount+'</div>';
            output += '<div class="descrizione , hyde">'+results[i].description+'</div>';
          }
        }  
        output += '</li></ul>'
        $('.results').append(output);
        $("form").on("submit",funzioneapi);

        const elemento = document.querySelectorAll('li');
        for(const box of elemento){
            box.addEventListener("click",funzione);
        }

        //funzioni per il cambio colore dei contenuti al passaggio del mouse
        const elemento2 = document.querySelectorAll('li');
        const elemento3 = document.querySelectorAll('li');
        for(const box2 of elemento2){
            box2.addEventListener("mouseover",function(){
              $(this).css("color","black").css("background-color"," rgb(25, 98, 255)").css("border","2px solid black");});
        }
        for(const box3 of elemento3){
            box3.addEventListener("mouseout",function(){
              $(this).css("border","2px solid white").css("color","white").css("background-color","rgba(0, 0, 0, 0.8)");});
        }

        //funzione che aggiunge i contenuti ad una data raccolta
        function funzione(event){
          if(arrayid.length===0){
            alert("Nessuna raccolta creata");
            window.location.replace("http://localhost/homework/home/home.php");
          }
          let elem=this.getElementsByClassName("immagine");
          let src=$(elem).attr("src");
          let elem2=this.getElementsByClassName("titolo");
          let title=$(elem2).html();
          let elem3=this.getElementsByClassName("prezzo");
          let price=$(elem3).html();
          let elem4=this.getElementsByClassName("pagine");
          let pages=$(elem4).html();
          let elem5=this.getElementsByClassName("descrizione");
          let description=$(elem5).html();

            if(description=="null"){
              description='Nessuna descrizione disponibile';
            }
            description = description.split("'").join("’");
            description = description.split("…").join("...");
            title = title.split("'").join("’");
            title = title.split("…").join("...");
            
            const body=document.querySelector("body");
            const h1=document.createElement("h1");
            h1.textContent="Scegli a quale raccolta aggiungere il contenuto";
            h1.classList.add('toremove');
            $(h1).css("background-color","darkred").css("border","2px white solid").css("cursor","pointer").css("width","600px");
            $(h1).css("border-radius","20px 20px 20px 20px").css("text-align","center");
            body.appendChild(h1);

          for(let i=0;i<arrayid.length;i++){
            
            let box=document.createElement("li");
            let bottone=document.createElement("div");
            let id=document.createElement("div");
            id.textContent=arrayid[i];
            id.classList.add('id');
            box.classList.add('bottone');
            bottone.textContent=arraytitle[i];
            
            box.appendChild(bottone);
            box.appendChild(id);
            body.appendChild(box);
            box.addEventListener("mouseover",function(){
              $(this).css("color","black").css("background-color"," rgb(25, 98, 255)").css("border","2px solid black");})
          box.addEventListener("mouseout",function(){
              $(this).css("border","2px solid white").css("color","white").css("background-color","rgba(0, 0, 0, 0.8)");});
            
            $(id).css("display","none");
            box.addEventListener("click",funzione4);
          }
          const results=document.querySelector(".results");
          $(results).css("display","none");
          const form=document.querySelector("form");
          $(form).css("display","none");

          //aggiunge il contenuto ad una raccolta tramite ajax
          function funzione4(event){
          let elem3=this.getElementsByClassName("id");
          let id=$(elem3).html();
          
          //update dell'immagine di default
          $.ajax({
            url: 'http://localhost/homework/server.php', 
            type:'post',
            data: { "id2":id,"immagine3": src
          }});

          //invio i dati del contenuto da aggiungere
          $.ajax({
            url: 'http://localhost/homework/server.php', 
            type:'post',
            data: { "id":id,"immagine": src, "titolo": title, "prezzo":price, "pagine":pages, "descrizione":description
          },
           success: function(result) {
            $(".bottone").css("display","none");
            $(results).css("display","block");
            $(form).css("display","block");
            $(".toremove").remove();  
            }
          });
          }

        }
    }})
  });


//riempie i vettori con le raccolte del database
function onJSON(json){
  const username=$('.username').attr('value');
  for(evento of json){
    if(evento.username===username){
      arraytitle.push(evento.titolo);
      arrayid.push(evento.id);
    }
  }
}

function onResponse(response){
  return response.json();
}

fetch("search2.php").then(onResponse).then(onJSON);
