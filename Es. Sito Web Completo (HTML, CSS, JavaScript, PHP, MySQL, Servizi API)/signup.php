<?php include('server.php') ?>
<html>
    <head>
        <title>Registrazione</title>
        <link rel="stylesheet" href="http://localhost/homework/css/css.css"/>
        <script src="signup.js" defer="true"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
    </head>
    <body>
        
        <main>
            <form name='nome_form' method='post'>
                <p id="titolo">Registrazione</p>
                <p><label>Nome <input type='text' class='nome'></label></p>
                <p><label>Cognome <input type='text' class='cognome'></label></p>
                <p><label>E-mail <input type='text' class='e-mail'></label></p>
                <p><label>Nome utente <input type='text' class='nomeutente'></label></p>
                <p><label>Password <input type='password' class='password'></label></p>
                <p><label>Conferma password <input type='password' class='password2'></label></p>
                <p><label>&nbsp;<input type='submit' class="reg_user" invia value="conferma"></label></p>

                <p>
  		        Sei già registrato? <a href="login.php">Accedi</a>
  	            </p>
            </form>
        </main>
    </body>
</html>
