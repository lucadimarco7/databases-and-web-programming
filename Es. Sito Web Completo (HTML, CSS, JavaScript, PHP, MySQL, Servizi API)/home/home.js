const elem=document.querySelector(".raccolta img");
elem.addEventListener("click",funzione);

let arraytitoli=[];

//funzioni per il cambio colore dei vari menù
const elem1=document.querySelectorAll("#menu p");
for(box of elem1){
    box.addEventListener("mouseover",function(){
        $(this).css("color","black").css("background-color","rgb(25, 98, 255)").css("border-left","8px solid black");})
    box.addEventListener("mouseout",function(){
        $(this).css("border-left","none").css("color","white").css("background-color","rgba(0, 0, 0, 0)");})
}

const elem2=document.querySelectorAll(".raccolta li");
for(box of elem2){
    box.addEventListener("mouseover",function dentro(){
        $(this).css("color","black").css("background-color","rgb(25, 98, 255)").css("border","2px solid black");})
    box.addEventListener("mouseout",function fuori(){
        $(this).css("border","2px solid white").css("color","white").css("background-color","rgba(0, 0, 0, 0.8)");})
}

const elem3=document.querySelectorAll(".contenuto li");
for(box of elem3){
    box.addEventListener("mouseover",function(){
        $(this).css("color","black").css("background-color","rgb(25, 98, 255)").css("border","2px solid black");})
    box.addEventListener("mouseout",function(){
        $(this).css("border","2px solid white").css("color","white").css("background-color","rgba(0, 0, 0, 0.8)");})
}


//Funzione che crea la sezione "crea nuova raccolta"
function funzione(){
    elem.removeEventListener("click",funzione);
    
    const raccolta=document.querySelector(".raccolta li");
    const form=document.createElement("form");
    const input=document.createElement("input");
    const input2=document.createElement("input");
    const div=document.createElement("div");
    const testo=document.createElement("testo");
    
    div.addEventListener("click",funzione2);

    input.value="Inserisci titolo qui...";
    input.addEventListener("focus",function(){
        input.value="";
    })
    input2.value="Inserisci url immagine qui...";
    input2.addEventListener("focus",function(){
        input2.value="";
    })
    div.textContent="Conferma";
    testo.textContent="Inserire 'default' nel campo immagine per inserire un'immagine di default dal server";

    form.appendChild(input);
    form.appendChild(input2);
    form.appendChild(div);
    form.appendChild(testo);
    raccolta.appendChild(form);

    elem.addEventListener("click", function(){
        $(form).css("display","none");
        elem.addEventListener("click",funzione);
    });

    const username=$('.username').attr('value');

    //Funzione che crea la raccolta tramite i dati forniti e inviati tramite ajax a server php
    function funzione2(){
        for(let i=0;i<arraytitoli.length;i++){
            if(arraytitoli[i]==input.value){
                alert("Raccolta già esistente");
                return;
            }
        }
        
        $.ajax({
            url: 'http://localhost/homework/server.php', 
            type:'post',
            data: { "immagine2": input2.value, "titolo": input.value,"username":username
          }
        })  
        location.reload();
    }
}

//Funzione che visualizza i contenuti di ogni raccolta
function funzione3(event){
        const h1=document.querySelector(".overlay h1");
        let titolo=this.getElementsByClassName("titolo");
        let titolo3=$(titolo).html();    
        h1.textContent=titolo3;
        $(h1).css("font-weight","bold");
        let elem=this.getElementsByClassName("id");

    //richiesta ajax dei contenuti corrispondenti all'id della raccolta cliccata
    $.ajax({
        url: 'home3.php', 
        type:'post',
       success: function(result) {
        let id=$(elem).html();
        let i=0;
        let obj = JSON.parse(result);
          const contenuto=document.querySelector(".contenuto");
          $('.results').css("display","none");
          $('.raccolta').css("display","none");
          const ritorna=document.createElement("div");
          ritorna.classList.add('ritorna');
          ritorna.textContent="Ritorna alla home";
          contenuto.appendChild(ritorna);
          $(ritorna).css("border","2px solid black");
          ritorna.addEventListener("click",function(){
            window.location.replace("home.php");
          })
          for(evento of obj){
            if(id===evento.id){
            i++;
            const elimina=document.createElement("li");
            const box=document.createElement("li");
            const immagine=document.createElement("img");
            const immagine2=document.createElement("img");
            const titolo=document.createElement("li2");
            const prezzo=document.createElement("div");
            const pagine=document.createElement("div");
            const descrizione=document.createElement("div");
            const titolo2=document.createElement("div");
            const id2=document.createElement("div");
            const br=document.createElement("br");
            elimina.textContent="Rimuovi dalla raccolta";
            elimina.value=evento.id;
            elimina.classList.add('bottone');
            elimina.id=i;
            immagine.src=evento.url;
            immagine2.src=evento.url;
            titolo.textContent=evento.titolo;
            prezzo.textContent="• Prezzo: "+evento.prezzo+"$";
            pagine.textContent="• Pagine: "+evento.pagine;
            if(evento.pagine==="0"){
                pagine.textContent="• Pagine: Cover";
            }
            descrizione.textContent="• Descrizione: "+evento.descrizione;
            descrizione.textContent = descrizione.textContent.split("â€™").join("'");
            descrizione.textContent = descrizione.textContent.split("â€").join("-");
            titolo.textContent = titolo.textContent.split("â€™").join("'");
            titolo.textContent = titolo.textContent.split("â€").join("-");
            titolo2.textContent=evento.titolo;
            titolo2.classList.add('titolo');
            id2.classList.add('id2');
            id2.textContent=evento.id;
            elimina.appendChild(titolo2);
            box.appendChild(immagine);
            box.appendChild(titolo);
            box.appendChild(id2);
            box.classList.add(i);
            const contenuto=document.querySelector(".contenuto");
            contenuto.appendChild(box);
            

            /*codice per mostrare informazioni aggiuntive sui contenuti*/
            const body=document.querySelector("body");
            const menu=document.querySelector("#menu");
            const content=document.querySelector(".content");
            const h1=document.createElement("h1");
            const container=document.createElement("div");
            const container2=document.createElement("div");
            box.addEventListener("click",function espandi(){
                h1.textContent=titolo.textContent;
                $(contenuto).css("display","none");
                $(menu).css("display","none");
                $(content).css("display","none");
                container.appendChild(h1);
                container2.appendChild(immagine2);
                container2.appendChild(descrizione);
                descrizione.appendChild(pagine);
                pagine.appendChild(prezzo);
                prezzo.appendChild(elimina);
                
                $(container).css("background-color","rgba(0,0,0,0.9)").css("margin","20px").css("border-radius","20px 20px 20px 20px").css("display","flex");
                $(container).css("flex-direction","column").css("align-items","center").css("border","2px solid white").css("cursor","default");
                $(container2).css("display","flex").css("font-size","20px").css("width","100%");
                container.appendChild(container2);
                body.appendChild(container);
                $('h1').css("text-align","center");
                $('img').css("margin","20px").css("display","flex");
                $('img').css("width","300px").css("border-radius","20px 20px 20px 20px").css("border","2px solid white");
                $(descrizione).css("margin","20px");
                $(pagine).css("margin-top","20px");
                $(prezzo).css("margin-top","20px");
                $(elimina).css("margin-top","60px").css("width","100%").css("height","25px").css("font-size","20px");
                $(".titolo").css("display","none");
                elimina.addEventListener("click",function funzione4(event){
                    let id=this.value;
                    let elem2=this.getElementsByClassName("titolo");
                    let title=$(elem2).html();
                    let a=$(this).attr("id");
                    $.ajax({
                        url: 'home3.php', 
                        type:'post',
                        data:{"titolo2":title,"id":id}
                      });
                      
                      const li=document.querySelectorAll(".bottone");
                      for(evento of li){
                          if(evento.id==a){
                          console.log(evento.id);
                          $("."+a).css("display","none");    
                        }
                      }
                      h1.textContent=titolo3;
                      $('img').css("margin","0px");
                      $(content).css("display","block");
                      $(contenuto).css("display","block");
                      $(menu).css("display","block");
                      body.removeChild(container);
                });
                box.removeEventListener("click",espandi);
                container.addEventListener("click",function riduci(){
                    h1.textContent=titolo3;
                    $('img').css("margin","0px").css("border","inherit");
                    $(content).css("display","block");
                    $(contenuto).css("display","block");
                    $(menu).css("display","block");
                    body.removeChild(container);
                    container.removeEventListener("click",riduci);
                    box.addEventListener("click",espandi);
                });
            })
            
            let elem3=document.querySelectorAll(".contenuto li");
            for(let box of elem3){
                box.addEventListener("mouseover",function(){
                    $(this).css("color","black").css("background-color","rgb(25, 98, 255)").css("border","2px solid black");})
                box.addEventListener("mouseout",function(){
                    $(this).css("border","2px solid white").css("color","white").css("background-color","rgba(0, 0, 0, 0.8)");})
            }

            $(".id2").css("display","none");
            $(".titolo").css("display","none");
            }
        }
        }
      });
}

//fetch che crea la sezione con tutte le raccolte presenti nel database associate all'username dell'utente corrente
function onJSON(json){
    const collezione=document.querySelector(".results");
    const username=$('.username').attr('value');
    for(evento of json){
        if(evento.username===username){
        arraytitoli.push(evento.titolo);
        const box=document.createElement("li");
        const immagine=document.createElement("img");
        const titolo=document.createElement("li2");
        const id=document.createElement("div");
        const elimina=document.createElement("div");
        immagine.src=evento.url;
        immagine.classList.add('contatore'); //mi serve per contare l'id
        titolo.textContent=evento.titolo;
        titolo.classList.add('titolo');
        id.classList.add('id');
        id.textContent=evento.id;
        immagine.onerror=function(){
            $.ajax({
                url: 'http://localhost/homework/server.php', 
                type:'post',
                data:{"value1":immagine.src,"value2":id.textContent},
               success: function(result) {
                   console.log(result);
                   window.location.replace('http://localhost/homework/home/home.php');
               }
              });
        }
        elimina.textContent="Elimina raccolta";
        elimina.value=evento.id;
        elimina.classList.add('bottone');
        box.appendChild(immagine);
        box.appendChild(titolo);
        box.appendChild(id);
        box.appendChild(elimina);
        collezione.appendChild(box);
        $(".id").css("display","none");
        const elemento = document.querySelectorAll(".bottone");
        for(const box1 of elemento){    
            box1.addEventListener("click",function(){
                //ajax che invia l'id della raccolta da eliminare
                let a=this.value;
                $.ajax({
                    url: 'home3.php', 
                    type:'post',
                    data:{"value":a},
                   success: function(result) {
                       console.log(result);
                   }
                  });
                            
                window.location.replace("home.php");
            });
        }
        const elemento2 = document.querySelectorAll(".results li");
        for(const box of elemento2){
            box.addEventListener("mouseover",function(){
                $(this).css("color","black").css("background-color","rgb(25, 98, 255)").css("border","2px solid black");})
            box.addEventListener("mouseout",function(){
                $(this).css("border","2px white solid").css("color","white").css("background-color","rgba(0, 0, 0, 0.8)");})
            box.addEventListener("click",funzione3);
        }
    }

}

if(arraytitoli.length==0){
    console.log("entra qua");
    const div=document.createElement("div");
    div.id='testo2';
    div.textContent="Nessuna raccolta creata";
    $(div).css("width","390px").css("text-align","center");
    collezione.appendChild(div);
}
}

function onResponse(response){  
    return response.json(); 
}


fetch("home2.php").then(onResponse).then(onJSON);