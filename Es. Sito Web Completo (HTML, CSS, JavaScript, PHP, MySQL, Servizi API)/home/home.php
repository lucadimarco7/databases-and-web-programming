<?php 
  session_start(); 

  if (!isset($_SESSION['nomeutente'])) {
  	$_SESSION['msg'] = "Devi prima effettuare l'accesso";
  	header('location: http://localhost/homework/login.php');
  }
  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['nomeutente']);
  	header("location: http://localhost/homework/login.php");
  }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
  <link rel="stylesheet" type="text/css" href="http://localhost/homework/css/css2.css">
  <script src="home.js" defer="true"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
</head>
<body>

<div class="content">

    <header>
	<div class="overlay"><h1><strong>Home Page</strong></h1></div>
    </header>

      <!-- menu -->
    <?php  if (isset($_SESSION['nomeutente'])) : ?>
        <div id="menu">
        <div class="centra"><div class="benvenuto">Benvenuto<br> <strong><?php echo $_SESSION['nomeutente']; ?></strong></div></div>
        <div class="username" value="<?php echo $_SESSION['nomeutente']; ?>"></div>
        <p><label><a href="home.php"> Home</a></label></p>
        <p><label><a href="http://localhost/homework/search/search.php"> Ricerca</a></label></p>
        <p><label><a href="home.php?logout='1'"> Logout</a></label></p>
        </div>
    <?php endif ?>
</div>

<div class="raccolta">
  <li>
  <img src="folder.png"/><br>
  Crea una nuova raccolta
  </li>
  <div id="testo2">Le tue raccolte:</div>
</div>

<div class="results">

</div>

<div class="contenuto"></div>

</div>
</body>
</html>